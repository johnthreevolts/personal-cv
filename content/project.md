+++
title = 'Project'
date = 2024-04-07T11:08:26+03:00
draft = false
+++
## Project Overview

During my tenure at Meditec spanning over two years, my primary focus was on the development of the Unified Contact Center Platform (VKCP). The Unified Contact Center Platform (VKCP) was initiated to enhance the availability, timeliness, and quality of operational services provided to the public, while also ensuring the functionality of a unified call center platform to meet the needs of state public administration.

### Project Objectives:

- Increase the efficiency and timeliness of the 112 emergency service.
- Provide services with full information availability about incidents in real-time.
- Improve the availability of the 112 emergency service.
- Establish a unified technological environment for receiving emergency reports (calls, eCalls, SMS, etc.).
- Implement quality control for the 112 emergency service.
- Modernize the resource management system for operational services.

## Solution Description

### Database

The backbone of our solution relies on Microsoft SQL Server 2022. Our database architecture predominantly consists of stored procedures, serving as the primary means of interaction. Any SQL code outside of stored procedure calls is minimal, primarily found in auxiliary applications.

Key Components:
- **MSSQL Server 2022**: Central database management system.
- **Stored Procedures**: Primary method of interaction.
- **Tables, Views, and User Defined Types**: Structured data elements.
- **Triggers**: Enforce database integrity and manage actions.
- **Microsoft SQL Server Data Tools (SSDT)**: Facilitates seamless database migrations.

### Backend

Our backend is structured around Windows Communication Foundation (WCF) operating in REST mode. Communication predominantly occurs through POST requests with JSON bodies carrying parameters. We operate within a distributed monolithic architecture, with distinct modules hosted within Internet Information Services (IIS). Notably, module communication is achieved by inheriting required logic internally, eschewing external communication methods like REST or queues.

Key Components:
- **WCF in REST Mode**: Facilitates communication.
- **ADO.NET**: Handles interaction with the database, chosen for its performance superiority in comparison to Entity Framework, particularly with complex join operations. Additionally, in earlier years, Entity Framework proved too slow for 7-8 level joins, prompting the adoption of ADO.NET. Dapper was not considered as it was not widely recognized at the time.
- **Integrations**: Seamless connections with third-party systems and government services, such as the country resident registry and the IRVIS CCTV system.

### Client

Our client application is more than just its technology stack. In addition to leveraging Windows Presentation Foundation (WPF) and ClickOnce for distribution, it embraces modern software design principles and libraries to enhance functionality and maintainability.

Key Components and Practices:
- **MVVM Pattern**: Employed to separate concerns and facilitate testability, ensuring a clean and maintainable codebase.
- **Unity Dependency Injection (DI)**: Used for managing dependencies and promoting loosely coupled components, enabling easier unit testing and extensibility.
- **Prism.\* Libraries**: Integrated to streamline development and provide tools for building modular and maintainable applications.
- **SignalR**: Utilized for real-time communication between the client and server, enabling efficient updates and notifications.
- **In-App Queue with Database Storage**: Developed internally to manage asynchronous tasks and improve overall system responsiveness and reliability.
- **Embedded Chrome**: Utilized within the application for rendering web pages developed with Vue.js by a separate team.

### Shared Components

Our shared components are built upon the following technologies and practices:

- **ABX Framework**: Proprietary in-house framework utilized for both client and backend components.
    - **Features**:
        - Helper utilities.
        - Windows and popup management system for WPF.
        - Extensions over ADO.NET and other functionalities.
        - Application level functionality abstraction, encompassing authentication, settings management, role management, and classifier CRUD operations, among others.
- **Deployment**: Managed via a series of batch scripts invoking MSBuild with specific parameters, ensuring application and backend publishing processes.

## List of Significant Tasks

### Solution Cleanup

During the solution cleanup process, several key tasks were performed to streamline and modernize the project:

- **Deleting Obsolete Projects**: Identified and removed unused or redundant projects.
- **Clearing Obsolete Binding Redirects**: Audited and removed accumulated binding redirects that were no longer necessary.
- **Updating Libraries**: Upgraded outdated library versions to fresh versions obtained from NuGet. This involved consolidating multiple versions of the same library and removing dependencies that were no longer in use or available.
- **Migrating to x64 Architecture**: Transitioned the project from Any CPU to x64 architecture for improved performance and compatibility.
- **Upgrading to .NET Standard 2.0**: Updated projects from using .NET Standard 1.3 to version 2.0 for enhanced compatibility and features.
- **Updating SDK and Project Files**: Upgraded all project files to the new SDK format and migrated references to package references. This also involved resolving issues with broken publish scripts caused by the new SDK.
- **Handling COM Libraries**: Adapted the solution to accommodate changes in handling COM library references, including using interop DLL libraries and stripping unused references.
- **Web Target Transformation**: Replaced legacy web target imports with SlowCheetah for transforming web configuration files.
- **Dotnet Publish**: Utilized for both backend and client publishing, replacing direct MSBuild invocation. Workarounds were implemented to ensure proper generation of Global.asax and web.config files.
- **Click-Once**: Replaced with MSIX as ClickOnce successor.
- **PowerShell Scripts for Deployment**: Rewrote deployment scripts in PowerShell for improved structure and compatibility with the updated solution.

### TaskRunner Platform Development

The TaskRunner platform is designed as a single .exe application, launched within Kubernetes with task-specific parameters. It is tailored to execute various tasks efficiently while adhering to the requirement of being active only during task execution, ensuring optimal resource utilization within the Kubernetes environment. The platform is developed using the latest .NET 7 framework and leverages the simplicity and performance benefits of Dapper for database interactions.

- **Single Executable**: The TaskRunner platform comprises a single executable file capable of executing different tasks based on provided parameters.
- **Dynamic Task Execution**: The platform reads environment variables and invocation parameters to determine the task to be executed, allowing for flexibility and versatility in task execution.
- **Efficient Resource Utilization**: Tasks are executed within the Kubernetes environment, ensuring that the platform is active only when invoked, thus minimizing resource consumption and maximizing efficiency.
- **Memory Profiling**: Memory profiling has been conducted to optimize memory usage for memory-intensive tasks, preventing unnecessary memory wastage.
- **.NET 7**: The platform is developed using the latest .NET framework version 7, providing access to new features and improvements.
- **Dapper**: Dapper is utilized for database interactions, offering lightweight and efficient ORM capabilities.

### JSON-based DSL for Call Plan Form Building

The JSON-based DSL (Domain-Specific Language) for call plan form building facilitates the creation of customizable call plans consisting of reusable components. This DSL enables the construction of call plan forms with added conditions for form interactivity, enhancing the flexibility and functionality of the call planning process.

- **DSL Enrichment**: The DSL is continuously enriched with new constructions such as EQ (Equal), GT (Greater Than), LT (Less Than), CONTAINS, and more. These constructions provide additional capabilities for defining conditions and interactions within the call plan forms.
- **Form Fragment Creation**: The DSL allows for the creation of form fragments, encompassing all CRUD related operations. This includes the management of complex SQL scripts, some exceeding 10,000 lines in size, with dynamic SQL manipulation.
- **JSON Schema Generation Tool**: To simplify JSON editing and ensure adherence to predefined structures, a JSON schema generation tool has been developed. Leveraging reflection, this tool automatically generates stub classes from classes with specific attributes, which are then used to generate JSON schema file.
- **JSON Creation**: JSON files were generated for different call plans, with close to 100 variations. These JSON files define the structure, components, and conditions of each call plan, allowing for easy customization and adaptation as per business requirements.

### Integration of Swagger for Endpoint Documentation

To address the absence of documentation for our backend endpoints, we integrated Swagger into our system. This involved creating a new project dedicated to Swagger implementation, as our existing endpoints in WCF couldn't be automatically read by Swagger.

- **Creation of a New Project**: A new project was initiated specifically for Swagger integration, developed using .NET 7 to leverage modern technologies.
- **Endpoint Information Retrieval**: Since Swagger couldn't automatically read our WCF endpoints, we utilized reflection to dynamically fetch endpoint information. With that info stub classes were generated and fed to Swagger.

### Technologies and Tooling

| Programming Languages | Frameworks and Libraries | Database Technologies | Development Tools |
|------------------------|---------------------------|-----------------------|-------------------|
| C#                     | .NET Framework            | SQL                   | Rider             |
| PowerShell             | .NET 7                    | MSSQL                 | Visual Studio     |
| Bash                   | .NET Standard             | SSDT                  | GIT               |
|                        | WCF                       | Beekeeper             | TFS               |
|                        | WPF                       | SSMS                  | Insomnia          |
|                        | REST                      | DB Forge Event Profiler| MITM-Proxy        |
|                        |                           | Rider (database-related functionality)| WSL2|


## Conclusion

With the successful launch of the project, our system now operates seamlessly across the entire Latvian territory, providing efficient and streamlined services.

Key achievements include:

- **Centralized Call Handling**: The implementation of the Unified Contact Center Platform (VKCP) has simplified the process of handling calls from various services. Calls are now routed to a single centralized location, eliminating the need for redirection and ensuring prompt and effective response to emergencies.
- **Integration of Various Systems**: VKCP has absorbed multiple disparate systems, consolidating them into a single, convenient platform. This integration has enabled the incorporation of diverse functionalities, into a unified interface, enhancing efficiency and convenience for users.

The successful implementation of the project marks a significant milestone in improving operational services across Latvia, facilitating better coordination, accessibility, and responsiveness.