+++
title = 'Main'
date = 2024-04-07T11:08:26+03:00
draft = false
+++
Hello, I’m Aleksandrs Trofimovs, a full-stack developer. I’m oriented on C# and ASP.NET Core. Read my CV below for more details. Drop me a message at at@boy.lv if you're interested in my expertise. Also, I'm interested in learning new things, so I may be interested in a job in another language stack.

{{% container %}}
{{% item %}}
## Experience

### .NET Developer at SIA "Meditec" (December 2021 - March 2024)

Working on the development of the Unified Contact Center Platform (VKCP), focusing on enhancing the efficiency and timeliness of the 112 emergency service.

Responsibilities include:
- Project cleanup
- Mentorship of less experienced colleagues
- Performance optimization
- Integration of third party API
- DSL development and configurations for it

### Full Stack Web Developer at Esteriol (July 2017 - September 2021)

Worked on a worldwide car parts wholesale project with millions of details accessible in six languages.

Responsibilities included:
- Raw SQL query optimization
- Intelligent search algorithm development
- New backend API development
- Internal financial statistics module development
- Warehouse detail processing module development
- Product SEO description generator development
- New product version release
{{% /item %}}
{{% item %}}
## Education

Daugavpils University (2014 - 2018)
- Software Engineer, Professional Bachelor's Degree in Information Technologies

## Languages

- Russian
- English
- Latvian

## Skills

- C#
- ASP.NET Core
- WPF
- SQL
- MSSQL/MySQL/PostgreSQL
- Docker
- Linux
{{% /item %}}
{{% /container %}}